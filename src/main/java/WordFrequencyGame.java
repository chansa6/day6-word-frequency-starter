import java.util.*;
import java.util.stream.Collectors;


public class WordFrequencyGame {
    private static final String SPACE_PATTERN = "\\s+";
    private static final String LINE_BREAK = "\n";
    private static final String CALCULATE_ERROR_MESSAGE = "Calculate Error";

    public String getResult(String sentence){
        try {
            return formatOutput(countWordsDescending(sentence));
        } catch (Exception e) {
            return CALCULATE_ERROR_MESSAGE;
        }
    }

    private List<WordInfo> countWordsDescending(String sentence) {
        List<String> words = List.of(sentence.split(SPACE_PATTERN));
        HashSet<String> deduplicate = new HashSet<>(words);
        return deduplicate.stream().map(word -> new WordInfo(word, Collections.frequency(words, word)))
                .sorted((wordInfo1, wordInfo2) -> wordInfo2.getWordCount() - wordInfo1.getWordCount())
                .collect(Collectors.toList());
    }

    private String formatOutput(List<WordInfo> wordInfoList) {
        return wordInfoList.stream()
                .map(wordInfo -> String.format("%s %d", wordInfo.getWord(), wordInfo.getWordCount()))
                .collect(Collectors.joining(LINE_BREAK));
    }
}
